<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCartRepository")
 */
class ShoppingCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", name="cart_order")
     */
    private $order;

     /**
     * @ORM\Column(type="float", name="cart_total")
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="shoppingCarts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LineCart", mappedBy="shoppingCart", orphanRemoval=true, cascade={"persist"})
     */
    private $lineCart;

    public function __construct()
    {
        $this->lineCart = new ArrayCollection();
    }


    public function getId()
    {
        return $this->id;
    }

    public function getOrder(): ?bool
    {
        return $this->order;
    }

    public function setOrder(bool $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|LineCart[]
     */
    public function getLineCart(): Collection
    {
        return $this->lineCart;
    }

    public function addLineCart(LineCart $lineCart): self
    {
        if (!$this->lineCart->contains($lineCart)) {
            $this->lineCart[] = $lineCart;
            $lineCart->setShoppingCart($this);
        }

        return $this;
    }

    public function removeLineCart(LineCart $lineCart): self
    {
        if ($this->lineCart->contains($lineCart)) {
            $this->lineCart->removeElement($lineCart);
            // set the owning side to null (unless already changed)
            if ($lineCart->getShoppingCart() === $this) {
                $lineCart->setShoppingCart(null);
            }
        }

        return $this;
    }
}
